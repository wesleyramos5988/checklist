import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManterTarefasComponent } from './manter-tarefas/manter-tarefas.component';
import { ManterContratosComponent } from './manter-contratos/manter-contratos.component';


const routes: Routes = [
  { path: 'tarefas', component: ManterTarefasComponent },
  { path: 'contratos', component: ManterContratosComponent },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
