import { TarefasService } from './../tarefas.service';
import { Component, OnInit } from '@angular/core';
import { Tarefa } from './tarefa';

@Component({
  selector: 'manter-tarefas',
  templateUrl: './manter-tarefas.component.html',
  styleUrls: ['./manter-tarefas.component.css']
})
export class ManterTarefasComponent implements OnInit {
  hoje = Date.now();
  todos:Array<Tarefa>;

  constructor(private tarefasService: TarefasService){}

  ngOnInit(): void {
    this.listar();
  }

  listar(){
      this.tarefasService.listar().subscribe((tarefas: Tarefa[]) =>{
       console.log("9");
       this.todos = tarefas;
  });
       console.log("2");
  }
  
}
   
