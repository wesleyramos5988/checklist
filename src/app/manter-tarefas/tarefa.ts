export class Tarefa {
  id: number;
  title: string;
  done: boolean;
}