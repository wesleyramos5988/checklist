import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastrarTodoComponent } from './cadastrar-todo.component';

describe('CadastrarTodoComponent', () => {
  let component: CadastrarTodoComponent;
  let fixture: ComponentFixture<CadastrarTodoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastrarTodoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastrarTodoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
