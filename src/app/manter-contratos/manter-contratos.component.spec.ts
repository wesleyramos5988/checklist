import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManterContratosComponent } from './manter-contratos.component';

describe('ManterContratosComponent', () => {
  let component: ManterContratosComponent;
  let fixture: ComponentFixture<ManterContratosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManterContratosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManterContratosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
