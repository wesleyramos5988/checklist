import { Todo } from 'src/models/todo.model';
import { TarefasService } from './../tarefas.service';
import { Component, OnInit, Input } from '@angular/core';
import { Tarefa } from '../manter-tarefas/tarefa';

@Component({
  selector: 'exibir-todo',
  templateUrl: './exibir-todo.component.html'
})
export class ExibirTodoComponent implements OnInit {
 
   @Input() todoDeEntrada:Todo;
 
   constructor(){}

   ngOnInit(): void {
     
   }
}  