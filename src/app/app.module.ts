import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { ManterTarefasComponent } from './manter-tarefas/manter-tarefas.component';
import { ManterContratosComponent } from './manter-contratos/manter-contratos.component';
import { FormComponent } from './form/form.component';
import {MatMenuModule, MatButtonModule} from '@angular/material';
import { MenuComponent } from './menu/menu.component'
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TarefasService } from './tarefas.service';
import { ExibirTodoComponent } from './exibir-todo/exibir-todo.component';
import { CadastrarTodoComponent } from './cadastrar-todo/cadastrar-todo.component';

@NgModule({
  declarations: [
    AppComponent,
    ManterTarefasComponent,
    ManterContratosComponent,
    FormComponent,
    MenuComponent,
    ExibirTodoComponent,
    CadastrarTodoComponent,
   
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatMenuModule,
    MatButtonModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule
   
  ],
  providers: [TarefasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
