import { Tarefa } from './manter-tarefas/tarefa';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class TarefasService {
  private serverUrl = 'http://demo3174313.mockable.io/tarefas';

  constructor(private http: HttpClient) { }

  listar(): Observable<Tarefa[]> {
    return this.http.get<Tarefa[]>(this.serverUrl);
  }

  add(tarefa) {
    return this.http.post<Tarefa[]>(this.serverUrl,tarefa);
  }


}

